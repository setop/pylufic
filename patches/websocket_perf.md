For a 25MB file download

over 64s, spend 15.7s in utf8 validation.

```
Fri Apr 10 21:47:24 2020    prof.bin

         64389249 function calls (64387060 primitive calls) in 64.519 seconds

   Ordered by: internal time
   List reduced from 1331 to 10 due to restriction <10>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
     3341   40.257    0.012   40.257    0.012 {method 'read' of '_ssl._SSLSocket' objects}
 44618993    8.506    0.000    8.506    0.000 ./.venv/lib/python3.7/site-packages/websocket/_utils.py:71(_decode)
       13    7.231    0.556   15.737    1.211 ./.venv/lib/python3.7/site-packages/websocket/_utils.py:80(_validate_utf8)
   650065    0.897    0.000    2.161    0.000 ./.venv/lib/python3.7/site-packages/Crypto/Hash/SHA256.py:71(__init__)
   390026    0.818    0.000    1.854    0.000 ./.venv/lib/python3.7/site-packages/Crypto/Util/strxor.py:47(strxor)
   520052    0.681    0.000    1.022    0.000 ./.venv/lib/python3.7/site-packages/Crypto/Hash/SHA256.py:82(update)
        1    0.594    0.594    0.594    0.594 {method 'do_handshake' of '_ssl._SSLSocket' objects}
   260026    0.411    0.000    0.660    0.000 ./.venv/lib/python3.7/site-packages/Crypto/Hash/SHA256.py:96(digest)
  1690364    0.407    0.000    0.885    0.000 ./.venv/lib/python3.7/site-packages/Crypto/Util/_raw_api.py:224(c_uint8_ptr)
   650143    0.353    0.000    0.458    0.000 /usr/lib/python3.7/ctypes/__init__.py:47(create_string_buffer)
```

after patch

```
Fri Apr 10 22:02:52 2020    prof.bin

         19770246 function calls (19768057 primitive calls) in 45.342 seconds

   Ordered by: internal time
   List reduced from 1328 to 10 due to restriction <10>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
     3341   35.718    0.011   35.718    0.011 {method 'read' of '_ssl._SSLSocket' objects}
   650065    1.008    0.000    2.410    0.000 ./.venv/lib/python3.7/site-packages/Crypto/Hash/SHA256.py:71(__init__)
   390026    0.929    0.000    2.093    0.000 ./.venv/lib/python3.7/site-packages/Crypto/Util/strxor.py:47(strxor)
   520052    0.756    0.000    1.141    0.000 ./.venv/lib/python3.7/site-packages/Crypto/Hash/SHA256.py:82(update)
       39    0.562    0.014    0.562    0.014 /usr/lib/python3.7/json/decoder.py:343(raw_decode)
   260026    0.472    0.000    0.755    0.000 ./.venv/lib/python3.7/site-packages/Crypto/Hash/SHA256.py:96(digest)
  1690364    0.457    0.000    0.994    0.000 ./.venv/lib/python3.7/site-packages/Crypto/Util/_raw_api.py:224(c_uint8_ptr)
   130013    0.410    0.000    4.669    0.000 ./.venv/lib/python3.7/site-packages/Crypto/Hash/HMAC.py:54(__init__)
   650143    0.396    0.000    0.515    0.000 /usr/lib/python3.7/ctypes/__init__.py:47(create_string_buffer)
        1    0.364    0.364    0.364    0.364 {method 'do_handshake' of '_ssl._SSLSocket' objects}
```
