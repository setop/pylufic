#!/bin/bash -e

PYPI_TEST="https://test.pypi.org/legacy/"
opts="--verbose"

rm -rfv dist/*
#python setup.py sdist bdist_wheel
python setup.py bdist_wheel
read -e -p "upload $(cat VERSION) to ? " -i $PYPI_TEST pypi
if [ ${#pypi} -gt 0 ]; then
    opts="$opts --repository-url"
fi
twine upload --repository pylufic $opts $pypi dist/*

