import setuptools

with open("./README.md", "r") as fh:
    long_description = fh.read()

with open("./requirements.txt", "r") as fh:
    install_requires = fh.read().splitlines()

with open("./VERSION", "r") as fh:
    version = fh.read()


setuptools.setup(
    name="pylufic",
    version=version,
    author="setop",
    author_email="setop@zoocoop.com",
    description="CLI client for lufi service",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://framagit.org/setop/pylufic",
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'pylufic=pylufic.pylufic:main',
        ],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
    ],
    install_requires = install_requires,
)
